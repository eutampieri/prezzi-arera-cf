use std::str::FromStr;

use cfg_if::cfg_if;

use chrono::prelude::*;
use chrono_tz::Europe::Rome;
use italian_energy_prices::Rate;

cfg_if! {
    // https://github.com/rustwasm/console_error_panic_hook#readme
    if #[cfg(feature = "console_error_panic_hook")] {
        extern crate console_error_panic_hook;
        pub use self::console_error_panic_hook::set_once as set_panic_hook;
    } else {
        #[inline]
        pub fn set_panic_hook() {}
    }
}

pub fn get_current_rate() -> Rate {
    let now = Rome
        .timestamp_millis_opt(worker::Date::now().as_millis().try_into().unwrap())
        .unwrap();
    match now.weekday() {
        Weekday::Sat => Rate::F3,
        Weekday::Sun => Rate::F3,
        _ => match now.hour() {
            0..=6 => Rate::F3,
            7 => Rate::F2,
            8..=18 => Rate::F1,
            19..=22 => Rate::F2,
            23 => Rate::F3,
            _ => panic!("This is not possible..."),
        },
    }
}

fn today() -> (u8, u8, u16) {
    let now = Rome
        .timestamp_millis_opt(worker::Date::now().as_millis().try_into().unwrap())
        .unwrap();
    (now.day() as u8, now.month() as u8, now.year() as u16)
}
fn date_within(date: (u8, u8, u16), end: (u8, u8, u16)) -> bool {
    if end.2 > date.2 {
        true
    } else if end.2 == end.2 {
        if end.1 > date.1 {
            true
        } else if end.1 == date.1 {
            end.0 >= date.0
        } else {
            false
        }
    } else {
        false
    }
}

pub fn today_in_range(date: (u8, u8, u16)) -> bool {
    date_within(today(), date)
}

pub fn rate_to_str(r: &Rate) -> &'static str {
    match r {
        Rate::F1 => "F1",
        Rate::F2 => "F2",
        Rate::F3 => "F3",
    }
}
pub async fn fetch_arera() -> Vec<u8> {
    worker::Fetch::Url(
        worker::Url::from_str(
            "https://www.arera.it/allegati/dati_documenti/prezzi/elettricita-domestici.xls",
        )
        .unwrap(),
    )
    .send()
    .await
    .unwrap()
    .bytes()
    .await
    .unwrap_or_default()
}

#[cfg(test)]
mod test {
    use super::date_within;

    #[test]
    fn test_current_day() {
        assert!(date_within((1, 1, 2022), (1, 1, 2022)))
    }
    #[test]
    fn test_current_month_witin() {
        assert!(date_within((1, 1, 2022), (2, 1, 2022)))
    }
    #[test]
    fn test_current_year_witin() {
        assert!(date_within((1, 1, 2022), (1, 2, 2022)))
    }
    #[test]
    fn test_current_month_not_witin() {
        assert!(!date_within((2, 1, 2022), (1, 1, 2022)))
    }
    #[test]
    fn test_current_year_not_witin() {
        assert!(!date_within((1, 2, 2022), (1, 1, 2022)))
    }
    #[test]
    fn test_future() {
        assert!(date_within((1, 1, 2022), (1, 1, 2023)))
    }
    #[test]
    fn test_past() {
        assert!(date_within((1, 1, 2022), (1, 1, 2021)))
    }
}
