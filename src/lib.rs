use once_cell::sync::OnceCell;
use serde::Serialize;
use std::sync::RwLock;
use utils::fetch_arera;

use worker::*;

static ARERA: OnceCell<RwLock<Vec<italian_energy_prices::Rates>>> = OnceCell::new();

mod utils;

fn log_request(req: &Request) {
    console_log!(
        "{} - [{}], located at: {:?}, within: {}",
        Date::now().to_string(),
        req.path(),
        req.cf().coordinates().unwrap_or_default(),
        req.cf().region().unwrap_or("unknown region".into())
    );
}

#[derive(Debug, Serialize)]
struct PricesResponse {
    single_rate: f32,
    rate: &'static str,
    price: f32,
    validity_end: (u8, u8, u16),
}

#[event(fetch)]
pub async fn main(req: Request, env: Env, _ctx: worker::Context) -> Result<Response> {
    log_request(&req);
    utils::set_panic_hook();
    let router = Router::new();
    if ARERA.get().is_none() {
        let data = fetch_arera().await;
        let data = std::io::Cursor::new(data);
        let prices = italian_energy_prices::load_first_household_rates(data);
        ARERA.set(RwLock::new(prices)).unwrap();
    } else if !utils::today_in_range(ARERA.get().unwrap().read().unwrap()[0].validity.to) {
        let prices = &mut *ARERA.get().unwrap().write().unwrap();
        let data = fetch_arera().await;
        let data = std::io::Cursor::new(data);
        prices.clear();
        prices.append(&mut italian_energy_prices::load_first_household_rates(data));
        drop(prices);
    }

    router
        .get("/", |_, _| {
            let prices = ARERA.get().unwrap().read().unwrap();
            let price = &prices[0];
            let rate = utils::get_current_rate();
            let base = price.pd
                + price.ppe
                + price.σ3
                + price.uc3
                + price.uc6[0]
                + price.asos
                + price.arim;

            Response::from_json(&PricesResponse {
                single_rate: price.pe_single_rate + base,
                //current_rate: 0.0,
                rate: utils::rate_to_str(&rate),
                price: price
                    .pe
                    .iter()
                    .find(|x| utils::rate_to_str(&x.0) == utils::rate_to_str(&rate))
                    .unwrap()
                    .1
                    + base,
                validity_end: price.validity.to,
            })
        })
        .get("/worker-version", |_, ctx| {
            let version = ctx.var("WORKERS_RS_VERSION")?.to_string();
            Response::ok(version)
        })
        .run(req, env)
        .await
}
